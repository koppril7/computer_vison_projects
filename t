[1mdiff --git a/Project_3/automatic.asv b/Project_3/automatic.asv[m
[1mdeleted file mode 100644[m
[1mindex b7d2763..0000000[m
[1m--- a/Project_3/automatic.asv[m
[1m+++ /dev/null[m
[36m@@ -1,24 +0,0 @@[m
[31m-clear;[m
[31m-[m
[31m-[m
[31m-image_1 = imread('images/Custom/frame.jpg');[m
[31m-image_2 = imread('images/Custom/rhoover.jpg');[m
[31m-image_1 = imresize(image_1, .125);[m
[31m-image_2 = imresize(image_2, .125);[m
[31m-[m
[31m-[y1, x1, z1] = size(image_1);[m
[31m-[m
[31m-image_1 = im2double(image_1);[m
[31m-image_2 = im2double(image_2);[m
[31m-[m
[31m-matches = match(image_1, image_2);[m
[31m-H = ransac(matches);[m
[31m-channels = size(image_1,3);[m
[31m-for c = 1:channels[m
[31m-new_image(:,:,c)  = crop(single_channel_mesh(H, image_1(:,:,c), image_2(:,:,c)));[m
[31m-% new_image(:, :, c) = crop(new_image(:, :, c));[m
[31m-[m
[31m-end[m
[31m-% subplot(1,2,2);[m
[31m-figure_2 = figure();[m
[31m- imshow(new_image)[m
\ No newline at end of file[m
[1mdiff --git a/Project_3/automatic.m b/Project_3/automatic.m[m
[1mindex 8bce921..50c9ad6 100644[m
[1m--- a/Project_3/automatic.m[m
[1m+++ b/Project_3/automatic.m[m
[36m@@ -1,8 +1,8 @@[m
 clear;[m
 [m
 [m
[31m-image_1 = imread('images/Custom/frame.jpg');[m
[31m-image_2 = imread('images/Custom/rhoover.jpg');[m
[32m+[m[32mimage_1 = imread('images/Yard/Y1.jpg');[m
[32m+[m[32mimage_2 = imread('images/Yard/Y3.jpg');[m
 image_1 = imresize(image_1, .25);[m
 image_2 = imresize(image_2, .25);[m
 [m
[1mdiff --git a/Project_3/generate_matrix.m b/Project_3/generate_matrix.m[m
[1mindex b00b55c..f224517 100644[m
[1m--- a/Project_3/generate_matrix.m[m
[1m+++ b/Project_3/generate_matrix.m[m
[36m@@ -3,11 +3,8 @@[m [mlen = size(x, 1);[m
 [m
 final = [];[m
 for idx = (1:len)[m
[31m-%      single_point_homo = [ x(idx), y(idx), 1, 0, 0, 0, -xp(idx)*x(idx), -xp(idx)*y(idx), -xp(idx);[m
[31m-%          0, 0, 0, x(idx), y(idx), 1, -yp(idx)*x(idx), -yp(idx)*y(idx), -yp(idx)];[m
[31m-        single_point_homo = [ xp(idx), yp(idx), 1, 0, 0, 0, -x(idx)*xp(idx), -x(idx)*yp(idx), -x(idx);[m
[32m+[m[32m    single_point_homo = [ xp(idx), yp(idx), 1, 0, 0, 0, -x(idx)*xp(idx), -x(idx)*yp(idx), -x(idx);[m[41m[m
         0, 0, 0, xp(idx), yp(idx), 1, -y(idx)*xp(idx), -y(idx)*yp(idx), -y(idx)];[m
[31m-[m
     final = [final;single_point_homo];[m
 end[m
 [m
[1mdiff --git a/Project_3/manual_script.m b/Project_3/manual_script.m[m
[1mindex 1a46891..b49f894 100644[m
[1m--- a/Project_3/manual_script.m[m
[1m+++ b/Project_3/manual_script.m[m
[36m@@ -1,23 +1,21 @@[m
 clear;[m
 image_1 = load_image('images/Custom/frame_side.jpg');[m
[31m-image_2 = load_image('images/Custom/rhoover.jpg');[m
[32m+[m[32mimage_2 = load_image('images/Tile/Tile_2.jpg');[m
 % image_1 = rgb2gray(image_1);[m
[31m-% image_2 = rgb2gray(image_2);[m
[31m-image_1 = imresize(image_1, 1);[m
[31m-image_2 = imresize(image_2, 1);[m
[32m+[m[32m%image_2 = rgb2gray(image_2);[m
[32m+[m[32mimage_1 = imresize(image_1, .125);[m
[32m+[m[32mimage_2 = imresize(image_2, .125);[m
 [y1, x1, z1] = size(image_1);[m
 [m
[31m-% new_image = generate_mesh(image_1, image_2);[m
[32m+[m[32m new_image = generate_mesh(image_1, image_2);[m
  figure1 = figure();[m
[31m- imshow(image_1);[m
[31m-% [m
[31m-% [m
[31m-% % % Make sure the first 4 points are on left image[m
[31m-% [x, y] = ginput(4);[m
[32m+[m[32m imshow(image_2);[m
[32m+[m
[32m+[m
[32m+[m[32m% % Make sure the first 4 points are on left image[m
 [x, y] = ginput(4);[m
[31m-% xp = xp - x1 - 10;[m
[31m-xp = [ 1; size(image_2, 2); size(image_2,2); 1];[m
[31m-yp = [ 1; 1; size(image_2,1); size(image_2,1)];[m
[32m+[m[32m[xp, yp] = ginput(4);[m
[32m+[m[32mxp = xp - x1 - 10;[m
 [m
 % Creates the nx9 matrix[m
 final = generate_matrix(xp,yp, x, y);[m
[36m@@ -33,7 +31,7 @@[m [mend[m
 H = H ./ H(9);[m
 H = reshape(H, [3, 3])';[m
 [m
[31m-channels = size(image_1,3);[m
[32m+[m[32mchannels = size(image_2,3);[m
 for c = 1:channels[m
 hybrid_image(:,:,c)  = crop(single_channel_mesh(H, image_1(:,:,c), image_2(:,:,c)));[m
 end[m
[1mdiff --git a/Project_3/single_channel_mesh.m b/Project_3/single_channel_mesh.m[m
[1mindex 9527123..3a0b78e 100644[m
[1m--- a/Project_3/single_channel_mesh.m[m
[1m+++ b/Project_3/single_channel_mesh.m[m
[36m@@ -1,47 +1,4 @@[m
 function new_image = single_channel_mesh(H, image_1, image_2)[m
[31m-% max2x = size(img2,2);[m
[31m-% max2y = size(img2,1);[m
[31m-% max1x = size(img1,2);[m
[31m-% max1y = size(img1,1); [m
[31m-% [m
[31m-% [x, y] = meshgrid(1:max2x, 1:max2y);[m
[31m-% [m
[31m-% new_points = H * [x(:), y(:), ones(size(x(:)))].';[m
[31m-% new_points = new_points ./ new_points(3,:);[m
[31m-% min_coord = [min(new_points(1,:)); min(new_points(2,:)); 0];[m
[31m-% new_points = new_points - abs(min_coord);[m
[31m-% [m
[31m-% [m
[31m-% [m
[31m-% xI = reshape(new_points(1,:), max2y, max2x);[m
[31m-% yI = reshape(new_points(2,:), max2y, max2x);[m
[31m-% [m
[31m-% img_shift = interp2(x,y, img2,xI,yI);[m
[31m-% imshow(img_shift);[m
[31m-% new_image = zeros(max1y * 3, max1x*3);[m
[31m-% [m
[31m-% new_image(max1y:max1y*2-1, 1:max1x) = img1;[m
[31m-% [m
[31m-% [m
[31m-% Hi = inv(H);[m
[31m-% [m
[31m-% for x = 1:max2x[m
[31m-%     for y = 1:max2y[m
[31m-%         [m
[31m-%         coord = [x; y; 1];[m
[31m-%         [m
[31m-%         coord_p = H * coord;[m
[31m-%         coord_p = coord_p ./ coord_p(3);[m
[31m-%         coord_p = round(coord_p);[m
[31m-%         coord_shift = round(coord_p - abs(min_coord));[m
[31m-%         [m
[31m-%         new_image(coord_p(2)+max1y, coord_p(1)) = img_shift(coord_shift(2)+1, coord_shift(1)+1);[m
[31m-%        [m
[31m-%         [m
[31m-%     end[m
[31m-% end[m
[31m-% end[m
[31m-[m
 [y1, x1] = size(image_1);[m
 [y2, x2] = size(image_2);[m
 z = 1;[m
[36m@@ -66,7 +23,7 @@[m [mfor y_idx = area+1:len(1)-area[m
         x_p = new_coord(1);[m
         y_p = new_coord(2);[m
  [m
[31m-        if (x_p > 0)[m
[32m+[m[32m        if (x_p > 1 && y_p > 1)[m[41m[m
         interpretation = interp2(image_2(y_idx - area:y_idx + area, x_idx - area:x_idx + area));[m
         [y,x]=size(interpretation);[m
         y = ceil(y/2);[m
